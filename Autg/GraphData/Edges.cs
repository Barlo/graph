﻿using QuickGraph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autg
{
    public class Edges
    {
        public Verticle Source { get; set; }
        public Verticle Target { get; set; }
        public int IsOne { get; set; }

        public Edges(Verticle source, Verticle target,int IsOne)
        {
            this.Source = source;
            this.Target = target;
            this.IsOne = IsOne;
        }
        
    }
   
}
