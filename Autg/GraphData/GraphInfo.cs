﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autg.GraphData
{
    public class GraphInfo
    {
        public string FileName { get; set; }
        public int VertexNo { get; set; }
        public string PathToFile { get; set; }
        public int DualGraphEdgeNo { get; set; }
        public int  GraphEdgeNo { get; set; }
        public int CompletiveGraphVertexNo { get; set; }
        public GraphInfo()
        {

        }
    }
}
