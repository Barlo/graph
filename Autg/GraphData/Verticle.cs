﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autg
{
    public class Verticle
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Point Center { get; set; }
        public string Name { get; set; }

        public Verticle(Point p)
        {
            this.X = p.X;
            this.Y = p.Y;
        }
        public Verticle(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
        public Verticle(int x, int y, Point p)
            :this(x,y)
        {
            this.Center = p;
        }
        public Verticle(string name)
        {
            this.Name = name;
        }
    }
}
