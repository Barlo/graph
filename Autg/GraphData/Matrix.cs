﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
namespace Autg
{
    public class Matrix
    {
        public int[,] EgdeMatrix { get; set; }
        public string PathToFile { get; set; }

        public Matrix(string Path)
        {
            this.PathToFile = Path;
            this.EgdeMatrix = ReadFromFile(this.PathToFile);
        }
        private int[,] ReadFromFile(string Path)
        {
            var count = File.ReadAllLines(Path).Count();
            int[,] Matrix = new int[count, count];
            string s = string.Empty;
            var connections = new List<string>();
            try
            {
                using (StreamReader sr = new StreamReader(Path))
                {
                    while ((s = sr.ReadLine()) != null)
                    {
                        connections.Add(s);
                    }
                }
                connections.ToArray<string>();
                for (int i = 0; i < Matrix.GetLength(0); i++)
                {
                    var row = connections[i];
                    for (int j = 0; j < Matrix.GetLength(1); j++)
                    {
                        Matrix[i, j] = int.Parse(row[j].ToString());
                    }
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return Matrix;
        }
    }
}
