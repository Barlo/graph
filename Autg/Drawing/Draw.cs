﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace Autg.Drawing
{
    public class Draw
    {
        public Panel CurrentPanel { get; set; }
        public Graphics Gph { get; set; }
        public Graphics BitmapGph { get; set; }
        public Pen One { get; set; }
        public Pen Zero { get; set; }
        public SolidBrush Brush { get; set; }
        public Graph Graph { get; set; }
        public Bitmap Btm { get; set; }
        public Draw(Graph graph)
        {
            this.Brush = new SolidBrush(Color.Black);
            this.One = new Pen(Color.Red, 2.0f);
            this.Zero = new Pen(Color.Blue, 2.0f);
            this.Graph = graph;
        }
        public void DrawGraph(Panel p1)
        {
            try
            {
                Graph.Vertex = new List<Verticle>();
                CurrentPanel = p1;
                Btm = new Bitmap(CurrentPanel.Width, CurrentPanel.Height);
                Gph = CurrentPanel.CreateGraphics();
                BitmapGph = Graphics.FromImage(Btm);
                BitmapGph.FillRectangle(new SolidBrush(Color.White), 0, 0, CurrentPanel.Width, CurrentPanel.Height);
                double a, b, r, xi, yi, fi, VertexNo;
                a = p1.Width / 2;
                b = p1.Height / 2;
                r = b - 30;
                VertexNo = this.Graph.GraphMatrix.EgdeMatrix.GetLength(0);
                fi = 2 * Math.PI * (0 - 1) / VertexNo;
                xi = r * Math.Cos(fi) + a;
                yi = r * Math.Sin(fi) + b;
                for (int i = 0; i < VertexNo; i++)
                {
                    fi = 2 * Math.PI * (i - 1) / VertexNo;
                    xi = r * Math.Cos(fi) + a;
                    yi = r * Math.Sin(fi) + b;
                    Graph.Vertex.Add(new Verticle((int)xi, (int)yi, Center((int)xi, (int)yi)));
                }
                Graph.CreateEdges(this.Graph, this.Graph.GraphMatrix);
                DrawEdge();
                DrawVerticle();
                SaveBitmap(this.Btm);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private Point Center(int x, int y)
        {
            return new Point() { X = (x + 5), Y = (y + 5) };
        }
        private void SaveBitmap(Bitmap btm)
        {
            try
            {
                string name = string.Format(@"{0}{1}", CurrentPanel.Name.Replace("Panel", string.Empty), ".png");
                string path = string.Format("{0}\\{1}", Graph.File.PathToDirectory, name);
                btm.Save(path, ImageFormat.Png);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
        private void DrawEdge()
        {
            Gph.Clear(Color.White);
            foreach (var item in Graph.Egde)
            {
                if (CurrentPanel.Name.Equals("MainPanel"))
                {
                    if (item.IsOne == 1)
                    {
                        Gph.DrawLine(One, item.Source.Center.X, item.Source.Center.Y, item.Target.Center.X, item.Target.Center.Y);
                        BitmapGph.DrawLine(One, item.Source.Center.X, item.Source.Center.Y, item.Target.Center.X, item.Target.Center.Y);
                    }
                    else
                    {
                        Gph.DrawLine(Zero, item.Source.Center.X, item.Source.Center.Y, item.Target.Center.X, item.Target.Center.Y);
                        BitmapGph.DrawLine(Zero, item.Source.Center.X, item.Source.Center.Y, item.Target.Center.X, item.Target.Center.Y);
                    }
                    
                }
                else if (CurrentPanel.Name.Equals("ZeroPanel") && item.IsOne == 0)
                {
                    Gph.DrawLine(Zero, item.Source.Center.X, item.Source.Center.Y, item.Target.Center.X, item.Target.Center.Y);
                    BitmapGph.DrawLine(Zero, item.Source.Center.X, item.Source.Center.Y, item.Target.Center.X, item.Target.Center.Y);
                }
                else if (CurrentPanel.Name.Equals("OnePanel") && item.IsOne == 1)
                {
                    Gph.DrawLine(One, item.Source.Center.X, item.Source.Center.Y, item.Target.Center.X, item.Target.Center.Y);
                    BitmapGph.DrawLine(One, item.Source.Center.X, item.Source.Center.Y, item.Target.Center.X, item.Target.Center.Y);
                }

            }
        }
        private void DrawVerticle()
        {
            foreach (var item in Graph.Vertex)
            {
                Gph.FillRectangle(Brush, item.X, item.Y, 10, 10);
                BitmapGph.FillRectangle(Brush, item.X, item.Y, 10, 10);
            }
        }
    }

}
