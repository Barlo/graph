﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using QuickGraph;
using System.Windows.Forms;
using Autg;

namespace Autg
{
    public class Graph
    {

        public IList<Verticle> Vertex { get; set; }
        public IList<Edges> Egde { get; set; }
        public Matrix GraphMatrix { get; set; }
        public int Size { get; set; }
        public Files File { get; set; }
        public Graph(Matrix GraphMatrix, int size,Files file)
        {
            this.GraphMatrix = GraphMatrix;
            this.Vertex = new List<Verticle>();
            this.Egde = new List<Edges>();
            this.Size = size;
            this.File = file;
        }
        public void CreateVerticles(Graph grh, Matrix mtx)
        {
            Vertex = new List<Verticle>();
            for (int i = 0; i < mtx.EgdeMatrix.GetLength(0); i++)
            {
                this.Vertex.Add(new Verticle((i + 1).ToString()));
            }
        }
        public void CreateEdges(Graph g, Matrix mtx)
        {
            g.Egde = new List<Edges>();
            try
            {
                for (int i = 0; i < mtx.EgdeMatrix.GetLength(0); i++)
                {
                    for (int j = 0; j < mtx.EgdeMatrix.GetLength(1); j++)
                    {
                        if (j > i && j!=i) 
                        {
                            g.Vertex.ToArray();
                            g.Egde.Add(new Edges(g.Vertex[i], g.Vertex[j],mtx.EgdeMatrix[i,j]));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

    }
}
