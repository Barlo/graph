﻿namespace Autg
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainPanel = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.graphInfo = new System.Windows.Forms.GroupBox();
            this.txbDualGraphVertexNo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txbCompletiveGraphVertexNo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txbGraphVertexNo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txbVertexNo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txbPathToFile = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txbFileName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCreate = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.brnLoad = new System.Windows.Forms.Button();
            this.btnDraw = new System.Windows.Forms.Button();
            this.ZeroPanel = new System.Windows.Forms.Panel();
            this.OnePanel = new System.Windows.Forms.Panel();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox1.SuspendLayout();
            this.graphInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainPanel
            // 
            this.MainPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MainPanel.BackColor = System.Drawing.Color.White;
            this.MainPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MainPanel.Location = new System.Drawing.Point(12, 12);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(802, 360);
            this.MainPanel.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.graphInfo);
            this.groupBox1.Controls.Add(this.btnCreate);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.brnLoad);
            this.groupBox1.Controls.Add(this.btnDraw);
            this.groupBox1.Location = new System.Drawing.Point(820, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 582);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Menu";
            // 
            // graphInfo
            // 
            this.graphInfo.Controls.Add(this.txbDualGraphVertexNo);
            this.graphInfo.Controls.Add(this.label6);
            this.graphInfo.Controls.Add(this.txbCompletiveGraphVertexNo);
            this.graphInfo.Controls.Add(this.label5);
            this.graphInfo.Controls.Add(this.txbGraphVertexNo);
            this.graphInfo.Controls.Add(this.label4);
            this.graphInfo.Controls.Add(this.txbVertexNo);
            this.graphInfo.Controls.Add(this.label3);
            this.graphInfo.Controls.Add(this.txbPathToFile);
            this.graphInfo.Controls.Add(this.label2);
            this.graphInfo.Controls.Add(this.txbFileName);
            this.graphInfo.Controls.Add(this.label1);
            this.graphInfo.Location = new System.Drawing.Point(11, 110);
            this.graphInfo.Name = "graphInfo";
            this.graphInfo.Size = new System.Drawing.Size(182, 264);
            this.graphInfo.TabIndex = 4;
            this.graphInfo.TabStop = false;
            this.graphInfo.Text = "Graf";
            // 
            // txbDualGraphVertexNo
            // 
            this.txbDualGraphVertexNo.Location = new System.Drawing.Point(6, 237);
            this.txbDualGraphVertexNo.Name = "txbDualGraphVertexNo";
            this.txbDualGraphVertexNo.Size = new System.Drawing.Size(111, 20);
            this.txbDualGraphVertexNo.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 220);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(125, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Dualny - Liczba krawędzi";
            // 
            // txbCompletiveGraphVertexNo
            // 
            this.txbCompletiveGraphVertexNo.Location = new System.Drawing.Point(6, 197);
            this.txbCompletiveGraphVertexNo.Name = "txbCompletiveGraphVertexNo";
            this.txbCompletiveGraphVertexNo.Size = new System.Drawing.Size(111, 20);
            this.txbCompletiveGraphVertexNo.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 180);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(151, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Dopełniajacy - liczba krawędzi";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // txbGraphVertexNo
            // 
            this.txbGraphVertexNo.Location = new System.Drawing.Point(6, 157);
            this.txbGraphVertexNo.Name = "txbGraphVertexNo";
            this.txbGraphVertexNo.Size = new System.Drawing.Size(111, 20);
            this.txbGraphVertexNo.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Graf - Liczba krawędzi";
            // 
            // txbVertexNo
            // 
            this.txbVertexNo.Location = new System.Drawing.Point(6, 117);
            this.txbVertexNo.Name = "txbVertexNo";
            this.txbVertexNo.Size = new System.Drawing.Size(111, 20);
            this.txbVertexNo.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Liczba Wierzchołkow";
            // 
            // txbPathToFile
            // 
            this.txbPathToFile.Location = new System.Drawing.Point(6, 77);
            this.txbPathToFile.Name = "txbPathToFile";
            this.txbPathToFile.Size = new System.Drawing.Size(170, 20);
            this.txbPathToFile.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Scieżka do pliku";
            // 
            // txbFileName
            // 
            this.txbFileName.Location = new System.Drawing.Point(6, 37);
            this.txbFileName.Name = "txbFileName";
            this.txbFileName.Size = new System.Drawing.Size(170, 20);
            this.txbFileName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nazwa pliku";
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(11, 17);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(183, 28);
            this.btnCreate.TabIndex = 3;
            this.btnCreate.Text = "Stwórz";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(11, 414);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(183, 28);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Otwórz Folder zawierajacy";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // brnLoad
            // 
            this.brnLoad.Location = new System.Drawing.Point(11, 69);
            this.brnLoad.Name = "brnLoad";
            this.brnLoad.Size = new System.Drawing.Size(183, 28);
            this.brnLoad.TabIndex = 1;
            this.brnLoad.Text = "Wczytaj";
            this.brnLoad.UseVisualStyleBackColor = true;
            this.brnLoad.Click += new System.EventHandler(this.brnLoad_Click);
            // 
            // btnDraw
            // 
            this.btnDraw.Location = new System.Drawing.Point(11, 380);
            this.btnDraw.Name = "btnDraw";
            this.btnDraw.Size = new System.Drawing.Size(183, 28);
            this.btnDraw.TabIndex = 0;
            this.btnDraw.Text = "Rysuj";
            this.btnDraw.UseVisualStyleBackColor = true;
            this.btnDraw.Click += new System.EventHandler(this.btnDraw_Click);
            // 
            // ZeroPanel
            // 
            this.ZeroPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ZeroPanel.BackColor = System.Drawing.Color.White;
            this.ZeroPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ZeroPanel.Location = new System.Drawing.Point(12, 378);
            this.ZeroPanel.Name = "ZeroPanel";
            this.ZeroPanel.Size = new System.Drawing.Size(385, 216);
            this.ZeroPanel.TabIndex = 3;
            // 
            // OnePanel
            // 
            this.OnePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OnePanel.BackColor = System.Drawing.Color.White;
            this.OnePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.OnePanel.Location = new System.Drawing.Point(429, 378);
            this.OnePanel.Name = "OnePanel";
            this.OnePanel.Size = new System.Drawing.Size(385, 216);
            this.OnePanel.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1032, 606);
            this.Controls.Add(this.OnePanel);
            this.Controls.Add(this.ZeroPanel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.MainPanel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResizeEnd += new System.EventHandler(this.Form1_ResizeEnd);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.groupBox1.ResumeLayout(false);
            this.graphInfo.ResumeLayout(false);
            this.graphInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel MainPanel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnDraw;
        private System.Windows.Forms.Panel ZeroPanel;
        private System.Windows.Forms.Panel OnePanel;
        private System.Windows.Forms.Button brnLoad;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.GroupBox graphInfo;
        private System.Windows.Forms.TextBox txbCompletiveGraphVertexNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txbGraphVertexNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txbVertexNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txbPathToFile;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txbFileName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txbDualGraphVertexNo;
        private System.Windows.Forms.Label label6;
    }
}

