﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autg
{
    public partial class CreateGraph : Form
    {
        public TextBox[,] table { get; set; }
        public IList<string> tabList { get; set; }
        public Matrix mtx{ get; set; }
        public Form1 graphToForm { get; set; }
        public CreateGraph()
        {
            InitializeComponent();
            panel1.AutoScroll = true;
        }

       
        public void ConvertToList()
        {
            double flag;
            tabList = new List<string>();
            for (int i = 0; i < table.GetLength(0); i++)
            {
                string s=string.Empty;
                for (int j = 0; j < table.GetLength(1); j++)
                {
                    try
                    {
                        if (new Regex(@"\d+").IsMatch(table[i, j].Text))
                        {
                            flag = double.Parse(table[i, j].Text);
                            if (flag == 1 || flag == 0)
                            {
                                s += flag;
                            }
                            else
                            {
                                s += "1";
                            }
                        }
                        else
                        {
                            s += "1";
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }


                }
                tabList.Add(s);
            }
        }
        public void SavetoFile(string path)
        {
            using (StreamWriter sw = new StreamWriter(string.Format(@"{0}/{1}.txt",path,DirName.Text)))
            {
                foreach (var item in tabList)
                {
                    sw.WriteLine(item);
                }
            }
        }

        public string CreateDir()
        {
            string path = string.Format(@"../../Data/{0}", DirName.Text);
            try
            {
                if(!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path); 
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return path;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            
            ConvertToList();
            SavetoFile(CreateDir());
            Close();
        }

        private void txbVertexNo_TextChanged(object sender, EventArgs e)
        {
            panel1.Controls.Clear();
            int VertexNo=0;
            if (!(string.IsNullOrEmpty(txbVertexNo.Text) && string.IsNullOrWhiteSpace(txbVertexNo.Text)))
            {
                try
                {
                    VertexNo = int.Parse(txbVertexNo.Text);
                    table = new TextBox[VertexNo, VertexNo];
                }
                catch (Exception ex)
                {
                    VertexNo = 0;
                    MessageBox.Show(ex.Message);
                }
                if (VertexNo!=0)
                {
                    Point position = new Point();
                    int space = 10;
                    for (int i = 0; i < VertexNo; i++)
                    {
                        for (int j = 0; j < VertexNo; j++)
                        {
                            int width = (j + 1) * 30;
                            int height = (i + 1) * 30;
                            TextBox txb =  new TextBox();
                            txb.Text = "0";
                            txb.Left = width;
                            txb.Top = height;
                            txb.Width = 20;
                            if (i == j)
                            {
                                txb.BackColor = Color.Gray;
                                txb.ReadOnly = true;
                            }
                            panel1.Controls.Add(txb);
                            table[i, j] = txb;
                        }
                    }
                }
            }
        }
    }
}
