﻿using Autg.Drawing;
using Autg.GraphData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Autg
{
    public partial class Form1 : Form
    {
        public Graph gph { get; set; }
        public Draw draw { get; set; }
        public Files file { get; set; }
        public OpenFileDialog ofd { get; set; }
        public GraphInfo GraphInf { get; set; }
        public FolderBrowserDialog fbd { get; set; }



        public Form1()
        {
            InitializeComponent();
            this.ofd = new OpenFileDialog();
            this.fbd = new FolderBrowserDialog();
        }
        
        private void btnDraw_Click(object sender, EventArgs e)
        {
            draw = new Draw(gph);
            draw.DrawGraph(MainPanel);
            draw.DrawGraph(OnePanel);
            draw.DrawGraph(ZeroPanel);
            GraphInf.VertexNo = gph.Vertex.Count();
            GraphInf.DualGraphEdgeNo = gph.Egde.Count();
            GraphInf.GraphEdgeNo = gph.Egde.Where(p => p.IsOne == 1).Count();
            GraphInf.CompletiveGraphVertexNo = gph.Egde.Where(p=>p.IsOne == 0).Count();
            DisplayInfo(GraphInf);
        }

        private void brnLoad_Click(object sender, EventArgs e)
        {
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                file = new Files()
                {
                    
                    PathToDirectory = Path.GetDirectoryName(ofd.FileName),
                    FileName = ofd.SafeFileName
                };
            }
            Matrix mtx = new Matrix(string.Format(@"{0}/{1}",file.PathToDirectory,file.FileName));
            gph = new Graph(mtx,20,file);
            GraphInf = new GraphInfo()
            {
                 FileName = gph.File.FileName,
                 PathToFile = gph.File.PathToDirectory,
            };
            DisplayInfo(GraphInf);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string path = this.GraphInf.PathToFile;
            Process.Start(path);

        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            CreateGraph frmCg = new CreateGraph();
            frmCg.ShowDialog();
        }

        public void DisplayInfo(GraphInfo graphInfo)
        {
            if (graphInfo.VertexNo == 0)
            {
                txbFileName.Text = graphInfo.FileName;
                txbPathToFile.Text = graphInfo.PathToFile;
            }
            else
            {
                txbVertexNo.Text = graphInfo.VertexNo.ToString();
                txbGraphVertexNo.Text = graphInfo.GraphEdgeNo.ToString();
                txbCompletiveGraphVertexNo.Text = graphInfo.CompletiveGraphVertexNo.ToString();
                txbDualGraphVertexNo.Text = graphInfo.DualGraphEdgeNo.ToString();
            }
            
        }
        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void Form1_ResizeEnd(object sender, EventArgs e)
        {
            
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            btnDraw_Click(sender, e);
        }
    }
}
